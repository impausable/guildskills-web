-module(guildskills_user_controller, [Req, SessionID]).
-compile(export_all).
-default(profile).

logout('GET', []) ->
    {redirect, "/user/login",
        [ mochiweb_cookies:cookie("user_id", "", [{path, "/"}]),
            mochiweb_cookies:cookie("session_id", "", [{path, "/"}]) ]}.

profile('GET', []) ->
	case Req:cookie("user_id") of
        undefined -> {redirect, "/user/login"};
        Id ->
            case boss_db:find(Id) of
                undefined -> {redirect, "/user/login"};
                User ->
                    case User:session_identifier() =:= Req:cookie("session_id") of
                        false -> {redirect, "/user/login"};
                        true -> 
                        	{ok, [
                        		{name, User:name()},
                        		{email, User:email()},
                        		{characters, User:gs_characters()}
                        		]}
                    end
            end
     end;
profile('GET', [Name]) ->
	case boss_db:find_first(gs_user, [{name, 'matches', "*" ++ Name}]) of
		undefined -> {output, "Not Found"};
		User ->
			case User:session_identifier() =:= Req:cookie("session_id") of
                        false -> 
                        	{ok, [
                        		{name, User:name()},
                        		{characters, User:gs_characters()}
                        		]};
                        true -> 
                        	{ok, [
                        		{name, User:name()},
                        		{email, User:email()},
                        		{characters, User:gs_characters()}
                        		]}
            end
    end.


login('GET', []) ->
	{ok, [{redirect, Req:header(referer)}]};

login('POST', []) ->
    case Req:param("email") of
	undefined ->
	    {json, [{error, "Need email"}]};
	Email ->
	    case boss_db:find_first(gs_user, [{email, 'matches', "*" ++ Email}]) of
		undefined ->
		    {json, [{error, "Bad email"}]};
		User ->
		    case Req:param("password") of
			undefined ->
			    {json, [{error, "Need password"}]};
			Password ->
			    case pw_auth:check_password(erlang:list_to_binary(Password), User:pass_hash(), User:pass_salt()) of
				true ->
				    case boss_session:set_session_data(SessionID,
								       user_name, User:name()) of
					ok ->
					    {redirect, proplists:get_value("redirect",
                        Req:post_params(), "/"), User:login_cookies()};
					{error, Reason} ->
					    {json, [{error, Reason}]}
				    end;
				false ->
				    {json, [{error, "Bad password"}]}
			    end
		    end
	    end
    end;

login(_, []) ->
    {json, [{error, "Please login"}]}.

register('GET', []) ->
	{ok, [{redirect, Req:header(referer)}]};

register('POST', []) ->
    case Req:param("email") of
	undefined ->
	    {json, [{error, "Need email"}]};
	Email ->
	    case Req:param("name") of
		undefined ->
		    {json, [{error, "Need name"}]};
		Name ->
		    case Req:param("password") of
			undefined ->
			    {json, [{error, "Need password"}]};
			Password ->
			    case boss_db:find_first(gs_user, [{email, 'equals', Email}]) of
				undefined ->
				    {pbkdf2, HexPass, Salt} = pw_auth:hash_password(erlang:list_to_binary(Password)),
				    User = gs_user:new(id, Name, Email, true, HexPass, Salt),
				    case User:save() of
					{error, [ErrorMessages]} ->
					    {json, [{error, ErrorMessages}]};
					{ok, _} ->
					    {redirect, proplists:get_value("redirect",
                        Req:post_params(), "/"), User:login_cookies()}
				    end;
				User ->
				    {json, [{error, "Conflict"}]}
			    end
		    end
	    end
    end;

register(_, []) ->
    {json, [{error, "Not supported"}]}.