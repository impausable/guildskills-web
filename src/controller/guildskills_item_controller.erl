-module(guildskills_item_controller, [Req]).
-export([search/2]).

search('GET', [Name]) ->
   Url = "http://wildheap.com/en/complete?q=",
   case httpc:request(get, {Url ++ http_uri:encode(Name), []}, [], []) of
        {ok, {{_, 200, _}, _, Body}} ->
               R = mochijson2:decode(Body),
               {ok, [{ items, clean(R)}]};
        {error, Reason} -> {error, Reason}
   end.

clean([H|T]) ->
   [magic(H)|clean(T)];
clean([]) ->
   [].

magic(Dic) ->
   {struct, Content} = Dic,
   Content.

