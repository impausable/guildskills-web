-module(guildskills_character_controller, [Req, SessionID]).
-compile(export_all).
-default(mine).

before_(_) ->
	utils:require_login(Req).

mine('GET', []) ->
	case Req:cookie("user_id") of
        undefined -> {redirect, "/user/login"};
        Id ->
            case boss_db:find(Id) of
                undefined -> {redirect, "/user/login"};
                User ->
					{ok, [{characters, User:gs_characters()}]}
			end
	end.

detail('GET', [Server, Name]) ->
	case boss_db:find_first(gs_character, [{server, 'equals', Server},{name, 'equals', Name}]) of
		undefined -> {ok, [{error, "Not Found"}]};
		Character ->
			Items = Character:gs_items(),
			{ok, [{name, Name},{server, Server},{items, Items}]}
	end.

new('GET', []) ->
	{ok, []}
