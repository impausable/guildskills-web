-module(gs_user, [Id, Name, Email, Active::boolean(), PassHash, PassSalt]).
-compile(export_all).
-table("users").
-has({gs_characters, many}).

-define(SECRET_STRING, "it's quite secret").

session_identifier() ->
    mochihex:to_hex(erlang:md5(?SECRET_STRING ++ Email)).

check_password(Password) ->
    pw_auth:check_password(Password, PassHash, PassSalt).

login_cookies() ->
    [ mochiweb_cookies:cookie("user_id", Id, [{path, "/"}]),
        mochiweb_cookies:cookie("session_id", session_identifier(), [{path, "/"}]) ].